/*************************************************************************
	> File Name: test.c
	> Author: llb
	> Mail: llb1008x@126.com 
	> Created Time: 2017年04月23日 星期日 23时02分05秒

	函数参数
 ************************************************************************/

#include<stdio.h>


int main()
{
	printf("hello world\n");

	int f=4;
	int g=5;
	float h=5.0f;

	printf("%d\n",f,g);
	printf("%d %d\n",f);
	printf("%d %f\n",h,g);

	return 0;
}
