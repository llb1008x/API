/*************************************************************************
	> File Name: test.c
	> Author: llb
	> Mail: llb1008x@126.com 
	> Created Time: 2017年07月18日 星期二 09时01分42秒
	可变数组的使用
 ************************************************************************/
#include <stdio.h>
#include <stdlib.h>

/*相关的函数
 *Array array_create(int init_size);
 *void array_free(Array *a);
 *int array_size(const Array *a);
 *int *array_at(Array *a,int index);
 *void array_inflate(Array *a,int more_size);
 * */

int main()
{
	printf("hello world\n");

	return 0;
}
