/*************************************************************************
	> File Name: link.h
	> Author: llb
	> Mail: llb1008x@126.com
	> Created Time: 2017年08月27日 星期日 21时53分23秒
 ************************************************************************/

#ifndef _LINK_H
#define _LINK_H

typedef struct _node{

    int value;
    struct _node *next;
}NODE;

#endif
