/*************************************************************************
	> File Name: test4.c
	> Author: llb
	> Mail: llb1008x@126.com
	> Created Time: 2017年06月25日 星期日 13时53分25秒

    字符，字符串,字符串数组
 ************************************************************************/

#include <stdio.h>

int main(int argc,char *argv[])
{  
    int ch;

    while((ch=getchar())!=EOF){

        putchar(ch);
    }

    printf("EOF\n");

    return 0;
}
