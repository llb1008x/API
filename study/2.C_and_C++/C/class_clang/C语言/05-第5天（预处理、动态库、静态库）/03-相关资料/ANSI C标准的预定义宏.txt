=======================================================================
ANSI C标准的预定义宏
	__FILE__			宏所在文件的源文件名 
	__LINE__			宏所在行的行号
	__DATE__			代码编译的日期
	__TIME__			代码编译的时间
	__STDC__			指示编译器是否执行ANSI C标准，如果是则其值为1
	__cplusplus			编译C++程序时该标识符被定义为1
=======================================================================
	__func__			函数名，C99引入了__func__，但其不是宏
	__FUNCTION__		函数名
	__PRETTY_FUNCTION__	函数名
	
标准预定义宏（Standard Predefined Macros）具体参考
    http://gcc.gnu.org/onlinedocs/cpp/Standard-Predefined-Macros.html
    http://gcc.gnu.org/onlinedocs/gcc/Function-Names.html#Function-Names

编译器预定义宏（GNU-, Microsoft-Specific Predefined Macros）具体参考	
    http://gcc.gnu.org/onlinedocs/cpp/Common-Predefined-Macros.html#Common-Predefined-Macros
    http://msdn.microsoft.com/en-us/library/b0084kay
	http://gcc.gnu.org/onlinedocs/cpp/System_002dspecific-Predefined-Macros.html#System_002dspecific-Predefined-Macros
	
