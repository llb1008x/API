#ifndef	_STACK_H_
#define _STACK_H_

#define  STACK_SIZE	10

int push(int data);
int pop(void);
int full(void);
int empty(void);

#endif	//_STACK_H_