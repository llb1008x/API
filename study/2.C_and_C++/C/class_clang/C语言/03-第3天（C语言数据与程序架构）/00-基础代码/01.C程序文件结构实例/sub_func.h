/*=========================================================================
  工程名称：C语言阶段
  组成文件：Sub_func.h
  功能描述：声明函数，以及定义宏
  程序分析：
  维护记录：2010-09-12 v1.0		SunPlusEdu
=========================================================================*/
#ifndef __Sub_func_H__
#define __Sub_func_H__

/*加、减、乘、除函数声明*/
extern float my_sum(float a,float b);
extern float my_dec(float a,float b);
extern float my_mux(float a,float b);
extern float my_dvi(float a,float b);


#endif